package pl.amanowicz.braintri.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Data transfer class for positions
 */

@Data
public class PositionDTO {

    private Integer id;
    @NotNull
    private String title;

}
