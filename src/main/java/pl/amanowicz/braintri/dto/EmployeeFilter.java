package pl.amanowicz.braintri.dto;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import pl.amanowicz.braintri.exception.ValidationException;
import pl.amanowicz.braintri.utils.Constants;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class EmployeeFilter {

    // filtering
    private String firstName;
    private String lastName;
    private String email;
    private String positionTitle;

    // sorting
    private Pageable pageable;

    public EmployeeFilter(){
        this.pageable = createPageRequest(null, null, null);
    }

    public EmployeeFilter(String firstName, String lastName, String email, String positionTitle, String sortParams[], Integer pageIndex, Integer pageSize) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.positionTitle = positionTitle;
        this.pageable = createPageRequest(sortParams, pageIndex, pageSize);
    }

    private Pageable createPageRequest(String sortParams[], Integer pageIndex, Integer pageSize) {
        Sort sort = Sort.by(Sort.Direction.ASC, "lastName", "firstName"); // default
        if (sortParams != null && sortParams.length > 0) {
            List<Sort.Order> orders = Arrays.stream(sortParams)
                    .filter(sortParam -> sortParam != null && !sortParam.isEmpty())
                    .map(this::fetchSortParam)
                    .collect(Collectors.toList());
            sort = Sort.by(orders);
        }
        return PageRequest.of(
                Optional.ofNullable(pageIndex).orElse(0),
                Optional.ofNullable(pageSize).orElse(Constants.DEFAULT_PAGE_SIZE),
                sort);
    }

    private Sort.Order fetchSortParam(String sortParam) {
        String[] compounds = sortParam.split(Constants.SORT_DELIMETER);
        if (compounds.length != 2 || !Constants.SORTABLE_EMPLOYEE_FIELDS.contains(compounds[0])
                || !Constants.SORT_ORDER_DICTIONARY.containsKey(compounds[1])) {
            throw new ValidationException("Invalid sort parameters");
        }
        return new Sort.Order(Constants.SORT_ORDER_DICTIONARY.get(compounds[1]), compounds[0]);
    }
}
