package pl.amanowicz.braintri.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Static metamodel of Position class
 * Solely for Specification usage
 * <p>
 * In usual environment this class would be automatically generated as described here:
 * https://docs.jboss.org/hibernate/entitymanager/3.5/reference/en/html/metamodel.html
 */
@StaticMetamodel(Position.class)
public class Position_ {
    public static volatile SingularAttribute<Position, Integer> id;
    public static volatile SingularAttribute<Position, String> title;
}
