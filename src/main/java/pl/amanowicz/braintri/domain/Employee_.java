package pl.amanowicz.braintri.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Static metamodel of Employee class
 * Solely for Specification usage
 *
 * In usual environment this class would be automatically generated as described here:
 * https://docs.jboss.org/hibernate/entitymanager/3.5/reference/en/html/metamodel.html
 */
@StaticMetamodel(Employee.class)
public class Employee_ {
    public static volatile SingularAttribute<Employee, Integer> id;
    public static volatile SingularAttribute<Employee, String> firstName;
    public static volatile SingularAttribute<Employee, String> lastName;
    public static volatile SingularAttribute<Employee, String> email;
    public static volatile SingularAttribute<Employee, Position> position;
}
