package pl.amanowicz.braintri.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Class representing positions at the company
 */
@Data
@Entity
@Table(name = "positions")
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @CreationTimestamp
    @Column(name = "create_time")
    @Setter(AccessLevel.NONE)
    private LocalDateTime createTime;

    @Size(max = 100)
    @Column(name = "title", nullable = false, length = 100, unique = true)
    private String title;

    public Position() {}

    public Position(String title){
        this.title = title;
    }

}
