package pl.amanowicz.braintri.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.amanowicz.braintri.domain.Employee;
import pl.amanowicz.braintri.domain.Position;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    Page<Employee> findAll(Pageable pageable);

    Page<Employee> findAll(Specification<Employee> spec, Pageable pageRequest);

    Long countEmployeesByPosition(Position position);

}
