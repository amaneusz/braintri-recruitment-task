package pl.amanowicz.braintri.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.amanowicz.braintri.domain.Position;

public interface PositionRepository extends JpaRepository<Position, Integer> {

    Position findByTitle(String title);

}
