package pl.amanowicz.braintri.service;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.amanowicz.braintri.domain.Employee;
import pl.amanowicz.braintri.domain.Employee_;
import pl.amanowicz.braintri.domain.Position;
import pl.amanowicz.braintri.domain.Position_;
import pl.amanowicz.braintri.dto.EmployeeDTO;
import pl.amanowicz.braintri.dto.EmployeeFilter;
import pl.amanowicz.braintri.exception.ResourceNotFoundException;
import pl.amanowicz.braintri.exception.ValidationException;
import pl.amanowicz.braintri.repository.EmployeeRepository;
import pl.amanowicz.braintri.repository.PositionRepository;

import javax.persistence.criteria.Predicate;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Transactional(readOnly = true)
    public Page<EmployeeDTO> getAllEmployees(EmployeeFilter employeeFilter) {
        log.info("*** getAllEmployees => start for filter: {}", employeeFilter);
        Specification<Employee> specification = createSpecification(employeeFilter);
        Page<Employee> employees;
        if (specification != null) {
            employees = employeeRepository.findAll(specification, employeeFilter.getPageable());
        } else {
            employees = employeeRepository.findAll(employeeFilter.getPageable());
        }
        log.info("*** getAllEmployees => returning page: {}", employees);
        return employees.map(this::convertEmployeeToDTO);
    }

    @Transactional(readOnly = true)
    public EmployeeDTO getEmployeeById(Integer id) {
        log.info("*** getEmployeeById => start for id {}", id);
        Optional<Employee> employee = employeeRepository.findById(id);
        if(!employee.isPresent()){
            throw new ResourceNotFoundException(String.format("Couldn't find employee with id: %d", id));
        }
        log.info("*** getEmployeeById => Returning employee with id {}", id);
        return employee.map(this::convertEmployeeToDTO).get();
    }

    @Transactional
    public EmployeeDTO createEmployee(@Valid EmployeeDTO employeeDTO) {
        log.info("*** createEmployee => start for dto {}", employeeDTO);
        Employee employee = convertDTOtoEmployee(employeeDTO);
        if (employee.getPosition().getId() == null && employee.getPosition().getTitle() != null) {
            Position position = positionRepository.findByTitle(employee.getPosition().getTitle());
            if (position == null) {
                throw new ValidationException(String.format("Position with title \"%s\" not found in DB", employee.getPosition().getTitle()));
            }
            employee.setPosition(position);
        }
        employeeRepository.save(employee);
        log.info("*** createEmployee => Returning employee with id: {}", employeeDTO.getId());
        return convertEmployeeToDTO(employee);
    }

    @Transactional
    public void deleteEmployee(Integer id) {
        Optional<Employee> employeeToDelete = employeeRepository.findById(id);
        if (!employeeToDelete.isPresent()) {
            throw new ValidationException(String.format("Employee with id %d does not exist", id));
        }
        employeeRepository.delete(employeeToDelete.get());
    }

    private EmployeeDTO convertEmployeeToDTO(Employee source) {
        EmployeeDTO target = new EmployeeDTO();
        modelMapper.map(source, target);
        return target;
    }

    private Employee convertDTOtoEmployee(EmployeeDTO source) {
        Employee target = new Employee();
        modelMapper.map(source, target);
        return target;
    }

    private Specification<Employee> createSpecification(EmployeeFilter filter) {
        if (filter == null) {
            throw new ValidationException("Invalid specification filter");
        }

        return (root, criteriaQuery, criteriaBuilder) -> {
            criteriaQuery.distinct(true);
            List<Predicate> predicates = new ArrayList<>();
            // prepare predicates
            Optional.ofNullable(filter.getFirstName()).ifPresent(fn -> predicates
                    .add(criteriaBuilder.equal(root.get(Employee_.firstName), fn)));
            Optional.ofNullable(filter.getLastName()).ifPresent(ln -> predicates
                    .add(criteriaBuilder.equal(root.get(Employee_.lastName), ln)));
            Optional.ofNullable(filter.getEmail()).ifPresent(e -> predicates
                    .add(criteriaBuilder.equal(root.get(Employee_.email), e)));
            Optional.ofNullable(filter.getPositionTitle()).ifPresent(pos -> predicates.
                    add(criteriaBuilder.equal(root.join(Employee_.position).get(Position_.title), pos)));
            // make conjunction
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }

}
