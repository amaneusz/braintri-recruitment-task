package pl.amanowicz.braintri.service;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.amanowicz.braintri.domain.Position;
import pl.amanowicz.braintri.dto.PositionDTO;
import pl.amanowicz.braintri.exception.ValidationException;
import pl.amanowicz.braintri.repository.EmployeeRepository;
import pl.amanowicz.braintri.repository.PositionRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Transactional(readOnly = true)
    public PositionDTO getPositionById(Integer id){
        log.info("*** getPositionById => starts for id: {}", id);
        Optional<Position> position = positionRepository.findById(id);
        if(!position.isPresent()){
            throw new ValidationException(String.format("Position with id %d does not exist", id));
        }
        log.info("*** getPositionById => Returning position with id: {}", id);
        return position.map(this::convertPositionToDTO).get();
    }

    @Transactional(readOnly = true)
    public List<PositionDTO> getAllPositions(){
        log.info("*** getAllPositions => starts");
        List<Position> positions = positionRepository.findAll();
        log.info("*** getAllPositions => returning {} existing positions", positions.size());
        return positions.stream().map(this::convertPositionToDTO).collect(Collectors.toList());
    }

    @Transactional
    public PositionDTO create(String positionTitle){
        log.info("*** create => starts for position title {}", positionTitle);
        Position positionWithTitle = positionRepository.findByTitle(positionTitle);
        if(positionWithTitle != null){
            throw new ValidationException(String.format("Position with title \"%s\" already exists", positionTitle));
        }
        PositionDTO saved = convertPositionToDTO(positionRepository.save(new Position(positionTitle)));
        log.info("*** create => Returning position with title {} and id {}", saved.getTitle(), saved.getId());
        return saved;
    }

    @Transactional(readOnly = true)
    public Map<PositionDTO, Long> getPositionsWithEmployeeCount(){
        log.info("*** getPositionsWithEmployeeCount => starts");
        List<Position> positions = positionRepository.findAll();
        log.info("*** getPositionsWithEmployeeCount => returning employee counts for {} existing positions", positions.size());
        return positions.stream().collect(Collectors.toMap(
                this::convertPositionToDTO,
                employeeRepository::countEmployeesByPosition
        ));
    }

    private PositionDTO convertPositionToDTO(Position source){
        PositionDTO target = new PositionDTO();
        modelMapper.map(source, target);
        return target;
    }

}
