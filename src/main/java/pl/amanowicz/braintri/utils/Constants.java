package pl.amanowicz.braintri.utils;

import org.springframework.data.domain.Sort;

import java.util.*;

public class Constants {

    // sorting
    public static final Integer DEFAULT_PAGE_SIZE = 8;
    public static final List<String> SORTABLE_EMPLOYEE_FIELDS = Arrays.asList("firstName", "lastName", "email");
    public static final String SORT_DELIMETER = "_";
    public static final Map<String, Sort.Direction> SORT_ORDER_DICTIONARY = new HashMap<String, Sort.Direction>() {
        { put("asc", Sort.Direction.ASC); put("desc", Sort.Direction.DESC); }
    };

    // filtering
    public static final List<String> FILTERABLE_EMPLOYEE_FIELDS = Arrays.asList("firstName", "lastName", "email");
    public static final String FILTER_DELIMETER = ":";
}
