package pl.amanowicz.braintri.exception;

public class ValidationException extends RuntimeException {

    public ValidationException(String message){
        super(message);
    }

}
