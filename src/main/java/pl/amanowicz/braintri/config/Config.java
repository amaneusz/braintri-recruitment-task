package pl.amanowicz.braintri.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Simple configuration bean
 */
@Configuration
public class Config {

    private static String DB_NAME = "company_db";
    private static String DB_USER = "sa"; // default
    private static String DB_PASS = "";   // default

    /**
     * H2 db provider bean
     * @return H2 data source
     */
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setName(DB_NAME)
                .addScript("sql/create.sql")
                .addScript("sql/insert.sql")
                .build();
    }

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}
