package pl.amanowicz.braintri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BraintriRecruitmentTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(BraintriRecruitmentTaskApplication.class, args);
	}
}
