package pl.amanowicz.braintri.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.amanowicz.braintri.domain.Position;
import pl.amanowicz.braintri.dto.PositionDTO;
import pl.amanowicz.braintri.service.PositionService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("positions")
public class PositionController {

    @Autowired
    private PositionService positionService;

    @GetMapping(value = "")
    public ResponseEntity<List<PositionDTO>> getAll(){
        return ResponseEntity.ok(positionService.getAllPositions());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PositionDTO> getOneById(@PathVariable Integer id){
        return ResponseEntity.ok(positionService.getPositionById(id));
    }

    @PostMapping(value = "")
    public ResponseEntity<PositionDTO> create(@RequestBody String positionTitle){
        return ResponseEntity.ok(positionService.create(positionTitle));
    }

    @GetMapping(value = "/employeeCount")
    public ResponseEntity<Map<PositionDTO, Long>> getPositionsWithEmployeeCount() {
        return ResponseEntity.ok(positionService.getPositionsWithEmployeeCount());
    }

}
