package pl.amanowicz.braintri.web;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.amanowicz.braintri.exception.ResourceNotFoundException;
import pl.amanowicz.braintri.exception.ValidationException;

import java.util.Optional;
import java.util.stream.Collectors;

@ControllerAdvice
@RestController
public class ExceptionController extends ResponseEntityExceptionHandler {

    private static final String DEFAULT_ERR = "An error occured";

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        String errorMsg = ex.getBindingResult().getFieldErrors().stream()
                .map(fieldError ->
                        String.format("\"%s\": %s",
                                fieldError.getField(),
                                fieldError.getDefaultMessage()))
                .collect(Collectors.joining("\n\t", "Validation failed for fields:\n\t", ""));

        return new ResponseEntity<>(errorMsg, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handle(ValidationException e) {
        return new ResponseEntity<>(Optional.ofNullable(e.getMessage()).orElse(DEFAULT_ERR), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<String> handle(ResourceNotFoundException e) {
        return new ResponseEntity<>(Optional.ofNullable(e.getMessage()).orElse(DEFAULT_ERR), HttpStatus.NOT_FOUND);
    }

}
