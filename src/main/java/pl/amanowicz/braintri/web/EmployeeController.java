package pl.amanowicz.braintri.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.amanowicz.braintri.dto.EmployeeDTO;
import pl.amanowicz.braintri.dto.EmployeeFilter;
import pl.amanowicz.braintri.service.EmployeeService;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping(value = "")
    public ResponseEntity<Page<EmployeeDTO>> getAll(
            @RequestParam(value = "firstName", required = false) String firstName,
            @RequestParam(value = "lastName", required = false) String lastName,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "position", required = false) String position,
            @RequestParam(value = "sort", required = false) String [] sortParameters,
            @RequestParam(value = "page", required = false) Integer pageIndex,
            @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        EmployeeFilter filter = new EmployeeFilter(firstName, lastName, email, position, sortParameters, pageIndex,pageSize);
        return ResponseEntity.ok(employeeService.getAllEmployees(filter));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<EmployeeDTO> getOneById(@PathVariable Integer id) {
        return ResponseEntity.ok(employeeService.getEmployeeById(id));
    }

    @PostMapping(value = "")
    public ResponseEntity<EmployeeDTO> create(@RequestBody @Valid EmployeeDTO employeeToCreate) {
        EmployeeDTO created = employeeService.createEmployee(employeeToCreate);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(created.getId()).toUri();
        return ResponseEntity.created(location).body(created);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        employeeService.deleteEmployee(id);
        return ResponseEntity.ok(String.format("Employee with id %d has been deleted", id));
    }

}
