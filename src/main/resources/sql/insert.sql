-- populate positions
INSERT INTO positions (title) VALUES
  ('tester'), ('developer'), ('project manager'), ('recruiter');

-- populate employees
INSERT INTO employees (first_name, last_name, email, position_id) VALUES
  ('Jan', 'Kowalski', 'jkowalski@firma.pl', (SELECT id FROM positions WHERE title = 'tester')),
  ('Adam', 'Małysz', 'amalysz@firma.pl', (SELECT id FROM positions WHERE title = 'tester')),
  ('Jon', 'Doe', 'jdoe@firma.pl', (SELECT id FROM positions WHERE title = 'developer')),
  ('Anna', 'Mucha', 'amucha@firma.pl', (SELECT id FROM positions WHERE title = 'developer')),
  ('Ewa', 'Nowak', 'enowak@firma.pl', (SELECT id FROM positions WHERE title = 'developer')),
  ('Zbigniew', 'Nabielak', 'znabielak@firma.pl', (SELECT id FROM positions WHERE title = 'project manager')),
  ('Wojtek', 'Maciejowicz', 'wmaciejowicz@firma.pl', (SELECT id FROM positions WHERE title = 'recruiter'));