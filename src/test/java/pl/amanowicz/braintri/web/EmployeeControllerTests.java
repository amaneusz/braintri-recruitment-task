package pl.amanowicz.braintri.web;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.amanowicz.braintri.domain.Employee;
import pl.amanowicz.braintri.dto.EmployeeDTO;
import pl.amanowicz.braintri.dto.EmployeeFilter;
import pl.amanowicz.braintri.dto.PositionDTO;
import pl.amanowicz.braintri.service.EmployeeService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeControllerTests.class)
@WebAppConfiguration
public class EmployeeControllerTests {

    @Autowired
    private MockMvc mvc;

    @Mock
    private EmployeeService employeeService;
    @InjectMocks
    private EmployeeController employeeController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders
                .standaloneSetup(employeeController).build();
    }

    @Test
    public void testGetAllEmployees() throws Exception {
        Page<EmployeeDTO> employees = new PageImpl<EmployeeDTO>(sampleEmployees());
        EmployeeFilter emptyFilter = new EmployeeFilter();
        BDDMockito.given(employeeService.getAllEmployees(emptyFilter)).willReturn(employees);

        mvc.perform(get("/employees")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[0].firstName", is("Jan")));
    }

    private List<EmployeeDTO> sampleEmployees() {
        EmployeeDTO employee1 = new EmployeeDTO();
        employee1.setId(100);
        employee1.setFirstName("Jan");
        employee1.setLastName("Kowalski");
        EmployeeDTO employee2 = new EmployeeDTO();
        employee2.setId(200);
        employee2.setFirstName("Krzysztof");
        employee2.setLastName("Ibisz");
        PositionDTO position1 = new PositionDTO();
        position1.setId(100);
        position1.setTitle("Mleczarz");
        PositionDTO position2 = new PositionDTO();
        position2.setId(200);
        position2.setTitle("Celebryta");
        employee1.setPosition(position1);
        employee2.setPosition(position2);
        return Arrays.asList(employee1, employee2);
    }

}
