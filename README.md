<h2>Uruchomienie aplikacji</h2>
* Aplikacja działa w oparciu o SpringBoot i narzędzie Maven
* W celu uruchomienia aplikacji konieczne jest:
    * posiadanie zainstalowanego JDK 8 (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
    * zainstalowanie narzędzia Maven (https://maven.apache.org/install.html)
    
* Uruchomienie aplikacji polega na wywołaniu komendy **mvn spring-boot:run**

<h2>Baza danych</h2>
* Wykorzystywana w projekcie baza danych to <a href="http://www.h2database.com/html/main.html">H2</a>.
* Jest to bazda SQLowa wbudowana w aplikację. Dzięki temu nie ma potrzeby instalacji jakiegokolwiek silnika bazodanowego, wszystko dzieje się w pamięci aplikacji.
* Możliwe jest przeglądanie bazy danych. Interfejs użytkownika dostępny jest pod adresem <span style="color:blue">http://localhost:8080/h2-console</span>.
* Wspomniany GUI dostępny jest jedynie po uruchomieniu aplikacji.
* Baza danych odświeża się co uruchomienie aplikacji. Domyślnie podczas uruchomienia do bazy dodawani są pracownicy oraz stanowiska zdefiniowani w ramach pliku *src/main/resources/sql/insert.sql*.

<h2>Endpointy</h2>
<h4>Lista pracowników</h4> 
**Metoda:** GET <br>
**Endpoint:** /employees <br>
<p>Wywołanie tego endpointu zwraca obiekt Pageable zawierający listę użytkowników.
Na kolekcji użytkowników zastosowano paginację, sortowanie oraz filtrowanie. Wszystkie z wymienionych mechanizmów mogą być sterowane przez query params wypisane poniżej</p>

* Paginacja
    * **page** (*Integer*) - indeks strony
    * **pageSize** (*Integer*) - liczba elementów na stronę    
* Sortowanie
    * **sort** (*String - dopuszczalne >= 1*) - wartość przekazywana w każdym parametrze **sort** musi mieć postać [pole]_[kierunek_sortowania], gdzie [pole] to wartość tekstowa należąca do zbioru {firstName, lastName, email}, a [kierunek_sortowania] należy do {asc, desc} (kolejno rosnący, malejący).
* Filtrowanie
    * **firstName** (*String*) - imię pracownika
    * **lastName** (*String*) - nazwisko pracownika
    * **email** (*String*) - email pracownika
    * **position** (*String*) - tytuł stanowiska pracownika

Domyślna paginacja zaczyna się od indeksu 0 i mieści 8 wyników na stronę.<br>
Domyślne sortowanie uwzględnia nazwisko i imię oraz rosnący kerunek sortowania (asc).<br>
Nie ma domyślnego filtrowania.<br><br>
Poniżej przedstawiono przykładowy link, którego wywołanie zwróci wszystkich pracowników, którzy mają pozycję "developer" posortowanych malejąco po polu "email":
*/employees?position=developer&sort=email_desc*
<h4>Nowy pracownik</h4>
**Metoda:** POST <br>
**Endpoint:** /employees <br>
Wywołanie tego endpointu tworzy nowego użytkownika w baie danych.<br>
W ciele requestu konieczne jest przekazanie obiektu JSON zawierające następujące pola:
* **firstName** (*String*, pole wymagane) - imię pracownika
* **lastName** (*String*, pole wymagane) - nazwisko pracownika
* **mail** (*String*, pole wymagane i unikalne) - mail pracownika
* **position** (*JSON Object*, pole wymagane) - stanowisko pracownika

Pole **position** musi być obiektem JSON o następujących atrybutach
* **title** (*String*, pole wymagane) - nazwa stanowiska

Wymagalność i unikalność pól w kontekście systemu jest obdłużona mechanizmem walidacji. Ponadto, przy tworzeniu pracownika musimy uwzględnić istniejącą już pocycję. Przekazanie tytułu pozycji, którego nie ma w systemi skutkuje błędem walidacyjnym.<br>
Błędy walidacyjne skutkują niepowodzeniem requestu oraz zwróceniem statusu 400 (Bad Rquest).
<h4>Usunięcie pracownika</h4>
**Metoda:** DELETE <br>
**Endpoint:** /employees/{id} <br>
Metoda usuwa pracownika o id przekazanym w URL.<br>
Jeśli id nie istnieje w systemie, zwracany zostaje status 404 (Not Found).
<h4>Pobranie pracownika</h4>
**Metoda:** GET <br>
**Endpoint:** /employees/{id} <br>
Metoda zwraca pracownika o id przekazanym w URL.<br>
Jeśli id nie istnieje w systemie, zwracany zostaje status 404 (Not Found).
<h4>Pobranie stanowisk</h4>
**Metoda:** GET <br>
**Endpoint:** /positions <br>
Zwraca wszystkie stanowiska w systemie. Nie zastosowano paginacji.
<h4>Pobranie stanowiska</h4>
**Metoda:** GET <br>
**Endpoint:** /positions/{id} <br>
Metoda zwraca stanowisko o id przekazanym w URL.<br>
Jeśli id nie istnieje w systemie, zwracany zostaje status 404 (Not Found).
<h4>Dodanie stanowiska</h4>
**Metoda:** POST <br>
**Endpoint:** /positions <br>
Metoda odpowiedzialna za dodanie stanowiska do systemu.
W ciele meotdy post przekazywany jest jedynie tytuł nowego stanowiska (*String*).
Jeśli stanowisko o danym tytule istnieje - następuje bład walidacji i zwracany jest status 400 (Bad Request).
<h4>Pobranie stanowisk i liczby pracowników</h4>
**Metoda:** GET <br>
**Endpoint:** /positions/employeeCount <br>
Wywołanie zwraca słownik, gdzie kluczami są stanowiska, a wartościami liczba pracowników do nich przypisanych.
Nie zastosowano paginacji.

